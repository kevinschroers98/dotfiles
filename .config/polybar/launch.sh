#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

for i in /sys/class/hwmon/hwmon*/temp*_input; do 
	name="$(cat ${i%_*}_label 2>/dev/null || echo $(basename ${i%_*}))";
	if [ $name = "junction" ]; then
		export TEMP_GPU="$i"
	elif [ $name = "Tdie" ]; then
		export TEMP_CPU="$i"
	fi
done

polybar --reload left &
polybar --reload right &
# Launch Polybar, using default config location ~/.config/polybar/config
#if type "xrandr"; then
#  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
#    MONITOR=$m polybar --reload example &
#  done
#else
#  polybar --reload example &
#fi
