local function map(mode, lhs, rhs, opts)
	local options = {noremap = true}
	if opts then options = vim.tbl_extend('force', options, opts) end
	vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- Keybindings
local opts = {noremap=true, silent=true, expr = true}
map('i', '<Tab>',       'v:lua.tab()'       , opts)
map('i', '<S-Tab>',     'v:lua.s_tab()'     , opts)
map('s', '<Tab>',       'v:lua.tab()'       , opts)
map('s', '<S-Tab>',     'v:lua.s_tab()'     , opts)
map('i', '<c-j>',       'v:lua.comp_next()' , opts)
map('i', '<c-k>',       'v:lua.comp_prev()' , opts)
map('i', '<bs>',        'v:lua.bs()'        , opts)

map('n', '<c-j>',       'gj'                , {noremap = false, silent = true})
map('n', '<c-k>',       'gk'                , {noremap = false, silent = true})
map('n', '<leader>w',   '<c-w>'             , {noremap = false, silent = true})
map('n', 'U',           '<c-r>'             , {noremap = false, silent = true})

opts = {noremap=true, silent=true}
map('n', '<esc>',       '<cmd>nohl<cr>'     , opts)
-- DAP
map('n', '<F8>',        '<cmd>lua require"dap".toggle_breakpoint()<cr>', opts)
map('n', '<F6>',        '<cmd>lua require"dap".continue()<cr>', opts)
map('n', '<F7>',        '<cmd>lua require"dap".step_over()<cr>', opts)
map('n', '<F19>',       '<cmd>lua require"dap".step_into()<cr>', opts)
map('n', '<F9>',        '<cmd>lua require("dapui").toggle()<cr>', opts)
-- Bufferline
map('n', ']b',          '<cmd>BufferLineCycleNext<cr>', opts)
map('n', '[b',          '<cmd>BufferLineCyclePrev<cr>', opts)
map('n', '<leader>bl',  '<cmd>BufferLineMoveNext<cr>', opts)
map('n', '<leader>bh',  '<cmd>BufferLineMovePrev<cr>', opts)
map('n', '<leader>be',  '<cmd>BufferLineSortByExtension<cr>', opts)
map('n', '<leader>bd',  '<cmd>BufferLineSortByDirectory<cr>', opts)
map('n', '<leader>gb',  '<cmd>BufferLinePick<cr>', opts)
map('n', '<leader>db',  '<cmd>BufferLinePickClose<cr>', opts)
map('n', '<leader>1',   '<cmd>BufferLineGoToBuffer 1<cr>', opts)
map('n', '<leader>2',   '<cmd>BufferLineGoToBuffer 2<cr>', opts)
map('n', '<leader>3',   '<cmd>BufferLineGoToBuffer 3<cr>', opts)
map('n', '<leader>4',   '<cmd>BufferLineGoToBuffer 4<cr>', opts)
map('n', '<leader>5',   '<cmd>BufferLineGoToBuffer 5<cr>', opts)
map('n', '<leader>6',   '<cmd>BufferLineGoToBuffer 6<cr>', opts)
map('n', '<leader>7',   '<cmd>BufferLineGoToBuffer 7<cr>', opts)
map('n', '<leader>8',   '<cmd>BufferLineGoToBuffer 8<cr>', opts)
map('n', '<leader>9',   '<cmd>BufferLineGoToBuffer 9<cr>', opts)
map('n', '<leader>0',   '<cmd>BufferLineGoToBuffer 0<cr>', opts)
-- Telescope
map('n', '<leader>ff',  '<cmd>lua require("telescope.builtin").find_files()<cr>', opts)
map('n', '<leader>fg',  '<cmd>lua require("telescope.builtin").live_grep()<cr>', opts)
map('n', '<leader>fb',  '<cmd>lua require("telescope.builtin").buffers()<cr>', opts)
map('n', '<leader>fq',  '<cmd>lua require("telescope.builtin").quickfix()<cr>', opts)
map('n', '<leader>gs',  '<cmd>lua require("telescope.builtin").git_status()<cr>', opts)
map('n', '<leader>gf',  '<cmd>lua require("telescope.builtin").git_files()<cr>', opts)
map('n', '<leader>gb',  '<cmd>lua require("telescope.builtin").git_branches()<cr>', opts)
map('n', '<leader>gc',  '<cmd>lua require("telescope.builtin").git_commits()<cr>', opts)
map('n', '<leader>ft',  '<cmd>TodoTelescope<cr>', opts)

map('n', 'gr',          '<cmd>lua require("telescope.builtin").lsp_references()<cr>', opts)
map('n', 'gd',          '<cmd>lua require("telescope.builtin").lsp_definitions()<cr>', opts)
map('n', 'gi',          '<cmd>lua require("telescope.builtin").lsp_implementations()<cr>', opts)
map('n', '<leader>gd',  '<cmd>lua require("telescope.builtin").lsp_workspace_diagnostics()<cr>', opts)
map('n', '<leader>ca',  '<cmd>lua require("telescope.builtin").lsp_code_actions()<cr>', opts)
-- LSP
map('n', 'gD',          '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
map('n', 'K',           '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
map('n', '<C-h>',       '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
-- map('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
-- map('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
-- map('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
map('n', '<leader>D',   '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
map('n', '<leader>rn',  '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
map('n', '<leader>e',   '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
map('n', '[d',          '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
map('n', ']d',          '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
map('n', '<leader>l',   '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
map("n", "<leader>q",   '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

-- vimtex
-- TODO: add other keymaps, leader l is blocked
map('n', '<leader>tl',   '<Plug>(vimtex-compile)', { noremap=false, silent=true })
-- Tree
map('n', '<C-n>',       ':NvimTreeToggle<cr>', opts)
-- Hop
map('n', '<leader>w',	'<cmd>lua require"hop".hint_words()<cr>', opts)
map('o', '<leader>w',	'<cmd>lua require"hop".hint_words()<cr>', opts)
map('n', '<leader>h',	'<cmd>lua require"hop".hint_char1()<cr>', opts)
map('o', '<leader>h',	'<cmd>lua require"hop".hint_char1()<cr>', opts)
