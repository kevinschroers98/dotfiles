local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities.textDocument.completion.completionItem.resolveSupport = {
	properties = {
		'documentation',
		'detail',
		'additionalTextEdits',
	}
}

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics, {
    	virtual_text = true,
    	signs = true,
    	update_in_insert = true,
    }
)

require'lspconfig'.rust_analyzer.setup {
	capabilities = capabilities,
}

local sumneko_root_path = vim.env.HOME .. '/Git/lua-language-server'
local sumneko_binary = sumneko_root_path.."/bin/Linux/lua-language-server"
local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

require'lspconfig'.sumneko_lua.setup {
	cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"},
	settings = {
		Lua = {
			runtime = {
				-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
				version = 'LuaJIT',
				-- Setup your lua path
				path = runtime_path,
			},
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				globals = {'vim'},
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file("", true),
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = {
				enable = false,
			},
		},
	},
}

require'lspconfig'.ccls.setup {
	--init_options = {
	--	compilationDatabaseDirectory = "build";
	--	index = {
	--		threads = 0;
	--	};
	--	clang = {
	--		excludeArgs = { "-frounding-math"} ;
	--	};
	--}

	capabilities = capabilities,
}

--require'lspconfig'.pylsp.setup {
--	capabilities = capabilities,
--}

require'lspconfig'.jdtls.setup {
	capabilities = capabilities,
	cmd = {
		"jdtls",
		"-Dlog.protocol=true",
		"-Dlog.level=ALL",
		'--add-modules=ALL-SYSTEM',
		'--add-opens java.base/java.util=ALL-UNNAMED',
		'--add-opens java.base/java.lang=ALL-UNNAMED',
		'-data', vim.fn.fnamemodify(vim.fn.getcwd(), ':p:h:h') .. '/data',
	},
	init_options = {
		workspace = "~/Projects/java"
	},
	root_dir = require'lspconfig'.util.root_pattern('pom.xml', '.git'),
	single_file_mode = true
}

--[[
require'lspconfig'.texlab.setup {
	capabilities = capabilities,
	settings = {
		texlab = {
			build = {
				args = {
					"-pdf",
					"-interaction=nonstopmode",
					"-synctex=1",
					"-pv",
					"%f",
				},
				executable = "latexmk",
				onSave = true,
				forwardSearchAfter = true,
			},
			--forwardSearch = {
			--	executable = "zathura",
			--	args = {
			--		"--synctex-forward",
			--		"%l:1:%f",
			--		"%p"
			--	}
			--},
			chktex = {
				onOpenAndSave = false,
			}
		}
	}
}
]]
