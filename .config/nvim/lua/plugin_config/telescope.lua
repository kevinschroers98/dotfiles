local actions = require('telescope.actions')
local telescope_actions = require("telescope.actions.set")

local fixfolds = {
	hidden = true,
	attach_mappings = function(_)
		telescope_actions.select:enhance({
			post = function()
				vim.cmd(":normal! zx")
			end,
		})
		return true
	end,
}

require('telescope').setup{
	defaults = {
		mappings = {
			i = {
				["<esc>"] = actions.close,
				["<c-n>"] = false,
				["<c-p>"] = false,
				["<C-j>"] = actions.move_selection_next,
				["<C-k>"] = actions.move_selection_previous,
			}
		}
	},
	pickers = {
		buffers = fixfolds,
		file_browser = fixfolds,
		find_files = fixfolds,
		git_files = fixfolds,
		git_status = fixfolds,
		git_branches = fixfolds,
		git_commits = fixfolds,
		grep_string = fixfolds,
		live_grep = fixfolds,
		oldfiles = fixfolds,
		quickfix = fixfolds,
		lsp_references = fixfolds,
		lsp_definitions = fixfolds,
		lsp_implementations = fixfolds,
		lsp_workspace_diagnostics = fixfolds,
		lsp_code_actions = fixfolds,
		-- I probably missed some
	},
}
