--TODO: update settings
--:h nvim-tree.setup
local g = vim.g

g.nvim_tree_auto_ignore_ft = { 'startify', 'dashboard' } -- empty by default, don't auto open tree on specific filetypes.
g.nvim_tree_git_hl = 1 -- 0 by default, will enable file highlight for git attributes (can be used without the icons).
g.nvim_tree_root_folder_modifier = ':~' -- This is the default. See :help filename-modifiers for more options
g.nvim_tree_width_allow_resize  = 0 -- 0 by default, will not resize the tree when opening a file
g.nvim_tree_add_trailing = 0 -- 0 by default, append a trailing slash to folder names
g.nvim_tree_group_empty = 1 --  0 by default, compact folders that only contain a single folder into one node in the file tree
g.nvim_tree_special_files = { 'README.md', 'Makefile', 'MAKEFILE' } --  List of filenames that gets highlighted with NvimTreeSpecialFile

require'nvim-tree'.setup {
	disable_netrw       = true, -- disables netrw completely
	hijack_netrw        = true, -- hijack netrw window on startup
	open_on_setup       = false, -- open the tree when running this setup function
	ignore_ft_on_setup  = {}, -- will not open on setup if the filetype is in this list
	open_on_tab         = false, -- opens the tree when changing/opening a new tab if the tree wasn't previously opened
	
	update_to_buf_dir   = { -- hijacks new directory buffers when they are opened.
		enable = true, -- enable the feature
		auto_open = true, -- allow to open the tree if it was previously closed
	},
	
	hijack_cursor       = true, -- hijack the cursor in the tree to put it at the start of the filename
	update_cwd          = false, -- updates the root directory of the tree on `DirChanged` (when you run `:cd` usually)
	
	diagnostics = { -- show lsp diagnostics in the signcolumn
		enable = true,
		icons = {
			hint = "",
			info = "",
			warning = "",
			error = "",
		}
	},
	
	update_focused_file = { -- update the focused file on `BufEnter`, un-collapses the folders recursively until it finds the file
		enable      = false, -- enables the feature
		-- update the root directory of the tree to the one of the folder containing the file if the file is not under the current root directory
		-- only relevant when `update_focused_file.enable` is true
		update_cwd  = false,
		-- list of buffer names / filetypes that will not update the cwd if the file isn't found under the current root directory
		-- only relevant when `update_focused_file.update_cwd` is true and `update_focused_file.enable` is true
		ignore_list = {}
	},
	
	system_open = { -- configuration options for the system open command (`s` in the tree by default)
		cmd  = nil, -- the command to run this, leaving nil should work in most cases
		args = {}, -- the command arguments as a list
	},
	git = {
		enable = true,
		ignore = true,
	},
	view = {
		width = 40, -- width of the window, can be either a number (columns) or a string in `%`, for left or right side placement
		height = 30, -- height of the window, can be either a number (columns) or a string in `%`, for top or bottom side placement
		hide_root_folder = false, -- Hide the root path of the current folder on top of the tree 
		side = 'left', -- side of the tree, can be one of 'left' | 'right' | 'top' | 'bottom'
		auto_resize = false, -- if true the tree will resize itself after opening a file
		mappings = {
			-- custom only false will merge the list with the default mappings
			-- if true, it will only use your list to set the mappings
			custom_only = false,
			-- list of mappings to set on the tree manually
			list = {}
		}
	},
	filters = {
		dotfiles = false,
		custom = {"target", ".git"},
	},
	actions = {
		open_file = {
			quit_on_open = true, -- 0 by default, closes the tree when you open a file
		},
	},
	renderer = {
		indent_markers = {
			enable = true,
		},
	},
}
