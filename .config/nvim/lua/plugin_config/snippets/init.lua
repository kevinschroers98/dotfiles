local ls = require'luasnip'
-- some shorthands...
local s = ls.s
local sn = ls.sn
local t = ls.t
local i = ls.i
local f = ls.f
local c = ls.c
local d = ls.d

-- Every unspecified option will be set to the default.
ls.config.set_config({
	history = false,
	enable_autosnippets = true,
})

ls.snippets = {
	java = {
		-- Very long example for a java class.
		s({trig="fn"}, {
			d(6, jdocsnip, {2, 4, 5}), t({"", ""}),
			c(1, {
				t({"public "}),
				t({"private "})
			}),
			c(2, {
				t({"void"}),
				t({"String"}),
				t({"char"}),
				t({"int"}),
				t({"double"}),
				t({"boolean"}),
				i(nil, {""}),
			}),
			t({" "}),
			i(3, {"myFunc"}),
			t({"("}), i(4), t({")"}),
			c(5, {
				t({""}),
				sn(nil, {
					t({""," throws "}),
					i(1)
				})
			}),
			t({" {", "\t"}),
			i(0),
			t({"", "}"})
		})
	}
}

--[[
-- Beside defining your own snippets you can also load snippets from "vscode-like" packages defining
-- that expose snippets in json files, for example <https://github.com/rafamadriz/friendly-snippets>.
-- Mind that this will extend  `ls.snippets` so you need to do it after your own snippets or you
-- will need to extend the table yourself instead of setting a new one.
]]

require "plugin_config/snippets/all"
require "plugin_config/snippets/sh"
require "plugin_config/snippets/tex"
require "plugin_config/snippets/rust"
--require('luasnip/loaders/from_vscode').load()
