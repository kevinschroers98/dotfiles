local ls = require'luasnip'
-- some shorthands...
local s = ls.s
local sn = ls.sn
local t = ls.t
local i = ls.i
local f = ls.f
local c = ls.c
local d = ls.d

local function is_math()
	return vim.fn["vimtex#syntax#in_mathzone"]() == 1
end

-- 'recursive' dynamic snippet. Expands to some text followed by itself.
local rec_item
rec_item = function()
	return sn(nil, {
		c(1, {
			-- Order is important, sn(...) first would cause infinite loop of expansion.
			t({""}),
			sn(nil, {t({"", "\t\\item "}), i(1), d(2, rec_item, {})}),
		}),
	});
end

local slice_string = function(str, start)
	local i, j = str:find("%d+", start)
	if (i == nil) then
		return {
			first = str:sub(start, #str)
		}
	end
	return {
		first = str:sub(start, i - 1),
		second = str:sub(j + 1, #str),
		start = j + 1,
	}
end

local smart_range = function(args, snip, old_state)
	local text = args[1][1] -- v_{p}(x_{1})
	if (text == nil or text == "") then
		return sn(nil, {t({"failed"})})
	end
	local strings = {}
	local index = 1
	while(true) do
		local res = slice_string(text, index)
		table.insert(strings, res.first)
		if (res.second ~= nil) then
			index = res.start
		else
			break
		end
	end
	local nodes = {}
	for k, v in ipairs(strings) do
		if (k == 1) then
			table.insert(nodes, f(function(args, snip, usr_args) return v end, {}))
			if(#(strings) > 1) then
				table.insert(nodes, i(1, "n"))
			end
		else
			table.insert(nodes, f(function(args) return v end, {}))
			if (k < #(strings)) then
				table.insert(nodes, f(function(args) return args[1][1] end, {1}))
			end
		end
	end
	return sn(nil, nodes)
end

ls.snippets["tex"] = {
	-- rec_ls is self-referencing. That makes this snippet 'infinite' eg. have as many
	-- \item as necessary by utilizing a choiceNode.
	s({trig = "ls"}, {
		t({"\\begin{itemize}", "\t\\item "}),
		i(1),
		d(2, rec_item, {}),
		t({"", "\\end{itemize}"}),
		i(0)
	}),
	s({trig = "lm", name = "Inline maths"}, {
		t({"\\("}), i(1), t({"\\)"})
	}),
	s({trig = "enum", name = "Enumarate-Umgebung"}, {
		t({"\\begin{enumerate}"}),
		c(1, {
			t({"[label=(\\arabic*)]"}),
			t({"[label=(\\roman*)]"}),
			t({"[label=(\\alph*)]"}),
		}),
		t({"", "\t\\item "}),
		i(2),
		d(3, rec_item, {}),
		t({"", "\\end{enumerate}"}),
		i(0)
	}),
	s({trig = "beg", name = "Begin-Umgebung"}, {
		t({"\\begin{"}),
		i(1),
		t({"}", "\t"}),
		i(2),
		t({"", "\\end{"}),
		f(function(args, s, f) return args[1][1] end, {1}),
		t({"}"}),
		i(0)
	}),
	s({trig = "eqnn", name = "Equation no number"}, {
		t({"\\begin{equation*}", "\t"}),
		i(1),
		t({"", "\\end{equation*}"})
	}),
	s({trig = "eq", name = "Equation"}, {
		t({"\\begin{equation}", "\t"}),
		i(1),
		t({"", "\\end{equation}"})
	}),
	s({trig = "alnn", name = "Align no number"}, {
		t({"\\begin{align*}", "\t"}),
		i(1),
		t({"", "\\end{align*}"})
	}),
	s({trig = "al", name = "Align"}, {
		t({"\\begin{align}", "\t"}),
		i(1),
		t({"\\end{align}"})
	}),
	s({trig = "sec", name = "Section"}, {
		t({"\\section{"}),
		i(1),
		t({"}", ""})
	}),
	s({trig = "ssec", name = "Sub-Section"}, {
		t({"\\subsection{"}),
		i(1),
		t({"}", ""})
	}),
	s({trig = "sssec", name = "Sub-Section"}, {
		t({"\\subsubsection{"}),
		i(1),
		t({"}", ""})
	}),
}

ls.autosnippets["tex"] = {
	s({trig = "^^", name = "Supscript", wordTrig = false}, {
		t({"^{"}),
		i(1),
		t({"}"})
	}, {condition = is_math}),
	s({trig = "__", name = "Subscript", wordTrig = false}, {
		t({"_{"}),
		i(1),
		t({"}"})
	}, {condition = is_math}),
	s({trig = "dots", name = "Dots"}, {
		t({"\\dots"}),
	}, {condition = is_math}),
	s({trig = "cdot", name = "Centered dot"}, {
		t({"\\cdot"}),
	}, {condition = is_math}),
	s({trig = "frac", name = "Fraction"}, {
		t({"\\frac{"}),
		i(1),
		t({"}{"}),
		i(2),
		t({"}"})
	}, {condition = is_math}),
	s({trig = "binom", name = "Binomial coefficient"}, {
		t({"\\binom{"}),
		i(1, "n"),
		t({"}{"}),
		i(2, "k"),
		t({"} "}),
	}, {condition = is_math}),
	s({trig = "rarr", name = "Implication"}, {
		t({"\\Rightarrow"}),
	}, {condition = is_math}),
	s({trig = "lrarr", name = "Equivalence"}, {
		t({"\\Leftrightarrow"}),
	}, {condition = is_math}),
	s({trig = "set", name = "Set"}, {
		t({"\\{"}),
		i(1),
		t({"\\}"})
	}, {condition = is_math}),
	s({trig = "tpl", name = "Tupel"}, {
		t({"("}),
		i(1),
		t({")"})
	}, {condition = is_math}),
	s({trig = "range", name = "Create range of numbers"}, {
		i(1, "1"),
		t({", \\dots, "}),
		i(0, "n")
	}, {condition = is_math}),
	s({trig = "irange", name = "Create range of indexes"}, {
		i(1, "x"),
		t({"_{"}),
		i(2, "1"),
		t({"}, \\dots, "}),
		f(function(args, s, f) return args[1][1] end, {1}),
		t({"_{"}),
		i(3, "n"),
		t({"}"}),
	}, {condition = is_math}),
	s({trig = "srange", name = "Smart range"}, {
		i(1, "x"),
		t({", \\dots, "}),
		d(2, smart_range, {1}),
	}, {condition = is_math}),
	s({trig = "min", name = "Minimum"}, {
		t({"\\min\\{"}),
		i(1),
		t({"\\}"}),
	}, {condition = is_math}),
	s({trig = "max", name = "Maximum"}, {
		t({"\\max\\{"}),
		i(1),
		t({"\\}"}),
	}, {condition = is_math}),
	s({trig = "sum", name = "Sum"}, {
		t({"\\sum_{"}),
		i(1, "i = 0"),
		t({"}^{"}),
		i(2, "\\infty"),
		t({"} "}),
	}, {condition = is_math}),
	s({trig = "prod", name = "Product"}, {
		t({"\\prod_{"}),
		i(1, "i = 0"),
		t({"}^{"}),
		i(2, "\\infty"),
		t({"} "}),
	}, {condition = is_math}),
	s({trig = "nl", name = "New line"}, {
		t({"\\\\", ""}),
	}, {condition = is_math}),
	s({trig = "bb", name = "mathbb"}, {
		t({"\\mathbb{"}),
		i(1, "N"),
		t({"} "})
	}, {condition = is_math}),
	s({trig = "equiv", name = "Equivalence"}, {
		t({"\\equiv_{"}),
		i(1, "m"),
		t({"} "})
	}, {condition = is_math}),
	s({trig = "neq", name = "Not equal"}, {
		t({"\\neq "}),
	}, {condition = is_math}),
	s({trig = "over", name = "Overset"}, {
		t({"\\overset{"}),
		i(1),
		t({"}{"}),
		i(2),
		t({"} "}),
	}, {condition = is_math}),
}

-- vim: ts=2
