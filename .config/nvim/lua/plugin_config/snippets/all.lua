local ls = require'luasnip'
-- some shorthands...
local s = ls.s
local sn = ls.sn
local t = ls.t
local i = ls.i
local f = ls.f
local c = ls.c
local d = ls.d

local function shebang(_, _)
  local cstring = vim.split(vim.bo.commentstring, '%s', true)[1]
  if cstring == '/*' then
    cstring = '//'
  end
  cstring = vim.trim(cstring)
  return sn(nil, {
    t(cstring),
    t('!/usr/bin/env '),
    i(1, vim.bo.filetype),
  })
end

ls.snippets["all"] = {
	s({trig = "sb", name = "Shebang", dscr = "Add a shebang"}, {
		d(1, shebang, {}),
		t({"", ""})
	}),
	-- Parsing snippets: First parameter: Snippet-Trigger, Second: Snippet body.
	-- Placeholders are parsed into choices with 1. the placeholder text(as a snippet) and 2. an empty string.
	-- This means they are not SELECTed like in other editors/Snippet engines.
	--ls.parser.parse_snippet({trig="lspsyn"}, "Wow! This ${1:Stuff} really ${2:works. ${3:Well, a bit.}}"),

	-- When wordTrig is set, snippets only expand as full words (lte won't expand, te will).
	--ls.parser.parse_snippet({trig = "te", wordTrig = true}, "${1:cond} ? ${2:true} : ${3:false}"),

	-- When regTrig is set, trig is treated like a pattern, this snippet will expand after any number.
	--ls.parser.parse_snippet({trig = "%d", regTrig = true, wordTrig = true}, "A Number!!"),

	-- The last entry of args passed to the user-function is the surrounding snippet.
	--s({trig = "a%d", regTrig = true, wordTrig = true}, {
	--	f(function(args) return {"Triggered with " .. args[1].trigger .. "."} end, {}),
	--	i(0)
	--}),
	---- It's possible to use capture-groups inside regex-triggers.
	--s({trig = "b(%d)", regTrig = true, wordTrig = true}, {
	--	f(function(args) return {"Captured Text: " .. args[1].captures[1] .. "."} end, {}),
	--	i(0)
	--})
}
