local ls = require'luasnip'
-- some shorthands...
local s = ls.s
local sn = ls.sn
local t = ls.t
local i = ls.i
local f = ls.f
local c = ls.c
local d = ls.d

local rec_item
rec_item = function()
	return sn(nil, {
		c(1, {
			-- Order is important, sn(...) first would cause infinite loop of expansion.
			t({""}),
			sn(nil, {t({"", "else", "\t"}), i(1), d(2, rec_item, {})}),
			sn(nil, {t({"", "elif [ "}), i(1), t({" ] then;", "\t"}), d(2, rec_item, {})}),
		}),
	});
end

local rec_case
rec_case = function()
	return sn(nil, {
		c(1, {
			-- Order is important, sn(...) first would cause infinite loop of expansion.
			t({""}),
			sn(nil, {t({"", "\t"}), i(1), t({")", "\t\t"}), i(2), t({"", "\t\t;;"}), d(3, rec_case, {})}),
		}),
	});
end

ls.snippets["sh"] = {
	s({trig = "if"}, {
		t({"if [ "}),
		i(1),
		t({" ]; then", "\t"}),
		i(2),
		d(3, rec_item, {}),
		t({"", "fi", ""})
	}),
	s({trig = "case"}, {
		t({"case "}),
		i(1),
		t({" in", "\t"}),
		i(2),
		t({")", "\t\t"}),
		i(3),
		t({"", "\t\t;;"}),
		d(4, rec_case, {}),
		t({"", "\t*)", "\t\t"}),
		i(5),
		t({"", "\t\t;;"}),
		t({"", "esac", ""})
	}),
}
