local ls = require'luasnip'
-- some shorthands...
local s = ls.s
local sn = ls.sn
local t = ls.t
local i = ls.i
local f = ls.f
local c = ls.c
local d = ls.d

ls.snippets["rust"] = {
	s({trig = "fn"}, {
		t({"fn "}),
		i(1),
		t({"("}),
		i(2),
		t({") "}),
		f(function(args, snip, user_arg)
			if args[1][1] ~= nil then
				if args[1][1] ~= '' then
					return "-> "
				end
			end
			return ""
		end, {3}),
		i(3),
		t({"{", "\t"}),
		i(4),
		t({"", "}",})
	}),
	s({trig = "new"}, {
		t({"fn new("}),
		i(1),
		t({") -> Self {", "\t"}),
		i(2),
		t({"", "}",})
	}),
}
