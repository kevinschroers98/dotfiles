local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
	packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

local packer = require('packer')
local use = packer.use

return require('packer').startup(function()
	-- Packer can manage itself
	use 'wbthomason/packer.nvim'

	use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}

	use {
		'nvim-telescope/telescope.nvim',
		requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
	}

	use {
		'kyazdani42/nvim-tree.lua',
		requires = {'kyazdani42/nvim-web-devicons'}
	}

	use "ahmedkhalf/project.nvim"
	use "folke/which-key.nvim"

	use {
		'lewis6991/gitsigns.nvim',
		requires = {
			'nvim-lua/plenary.nvim'
		}
	}

	use 'neovim/nvim-lspconfig'

	use  {
		'mfussenegger/nvim-dap',
		requires = {{'jbyuki/one-small-step-for-vimkind'}, {'theHamsta/nvim-dap-virtual-text'}, {'rcarriga/nvim-dap-ui'}}
	}

	use {
		'akinsho/bufferline.nvim',
		requires = 'kyazdani42/nvim-web-devicons'
	}

	use {
		'hrsh7th/nvim-cmp',
		requires = {
			'hrsh7th/cmp-nvim-lsp',
			'hrsh7th/cmp-buffer',
			'hrsh7th/cmp-path',
			'hrsh7th/cmp-cmdline',
			'onsails/lspkind-nvim',
			'saadparwaiz1/cmp_luasnip',
		},
	}

	--use 'L3MON4D3/LuaSnip'
	use({"L3MON4D3/Luasnip", branch = "ls_snippets_preserve"})

	use 'phaazon/hop.nvim'

	use 'windwp/nvim-autopairs'

	use {
		'nvim-neorg/neorg',
		requires = "nvim-lua/plenary.nvim",
	}

	-- use { TODO: write for neorg
	-- 	'https://gitlab.com/thlamb/telescope-zettel.nvim',
	-- 	requires = {{'nvim-telescope/telescope.nvim'}, {'nvim-lua/plenary.nvim'}},
	-- 	config = function ()
	-- 		require"telescope".load_extension("zettel")
	-- 	end,
	-- }

	use 'lervag/vimtex'

	use {
		'TimUntersberger/neogit',
		requires = { 'nvim-lua/plenary.nvim', 'sindrets/diffview.nvim' },
	}

	use {
		"folke/todo-comments.nvim",
		requires = "nvim-lua/plenary.nvim",
	}

	use 'https://gitlab.com/RgrRgr/nvim-tab'
	-- use '~/Git/nvim-tab'
	use 'lukas-reineke/indent-blankline.nvim'
	-- use 'https://gitlab.com/RgrRgr/oceanic-next-lua'
	use 'marko-cerovac/material.nvim'
end)

