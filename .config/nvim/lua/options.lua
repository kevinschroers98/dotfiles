local cmd = vim.cmd
--local fn = vim.fn
local g = vim.g
local scopes = {o = vim.o, b = vim.bo, w = vim.wo}

local function opt(scope, key, value)
	scopes[scope][key] = value
	if scope ~= 'p' then scopes['o'][key] = value end
end

g.tex_conceal = 'abdmgs'
g.mapleader = ' '

-- cmd 'colorscheme oceanic-next'
cmd 'colorscheme material'
-- Configure tabs
local indent = 4
opt('b', 'expandtab', false)                        -- Use tabs instead of spaces
opt('b', 'shiftwidth', 0)                           -- Size of an indent
opt('b', 'tabstop', indent)                         -- Number of spaces tabs count for
opt('b', 'softtabstop', 0)                          -- Number of spaces tabs count for
opt('b', 'copyindent', true)                        -- Size of an indent
opt('b', 'preserveindent', true)                    -- Size of an indent
--opt('b', 'smartindent', true)                     -- Insert indents automatically
opt('b', 'indentexpr', 'indentexpr=nvim_treesitter#indent')                       -- Insert indents automatically

opt('o', 'hidden', true)                            -- Enable modified buffers in background
opt('o', 'ignorecase', true)                        -- Ignore case
opt('o', 'smartcase', true)                         -- Smart case
opt('o', 'joinspaces', false)                       -- No double spaces with join after a dot
opt('o', 'scrolloff', 10)                           -- Lines of context
opt('o', 'shiftround', true)                        -- Round indent
opt('o', 'splitbelow', true)                        -- Put new windows below current
opt('o', 'splitright', true)                        -- Put new windows right of current
opt('o', 'termguicolors', true)                     -- True color support
--opt('o', 'wildmode', 'list:longest')              -- Command-line completion mode

opt('w', 'conceallevel', 2)                         -- Set conceal level
opt('w', 'linebreak', true)                         -- Don't warp lines in words
opt('w', 'breakindent', true)                       -- Indent wrapped lines
opt('w', 'listchars', 'tab:  ,trail:+')             -- Show some invisible characters (tabs...)
opt('w', 'list', true)                              -- Show some invisible characters (tabs...)
opt('w', 'number', true)                            -- Print line number
opt('w', 'relativenumber', true)                    -- Relative line numbers
opt('w', 'signcolumn', 'yes')                       -- Relative line numbers
opt('w', 'wrap', true)                              -- Enable line wrap
opt('w', 'cursorline', true)                        -- Highlight cursorline
opt('w', 'cursorcolumn', true)                      -- Highlight cursorcolumn
opt('w', 'foldmethod', 'expr')                      -- Folds with
opt('w', 'foldexpr', 'nvim_treesitter#foldexpr()')  -- Treesitter
opt('o', 'foldlevelstart', 99)                      -- Disable folds on file open

opt('o', 'syntax', 'enable')                        -- Enable syntax
opt('o', 'encoding', "utf-8")                       -- Set encoding
opt('o', 'fileencoding', "utf-8")                   -- Set encoding
opt('o', 'guicursor', 'a:block')                    -- Set cursor style
opt('o', 'completeopt', 'menuone,noselect,preview') -- Completion options
