if status is-interactive
    # Commands to run in interactive sessions can go here
    starship init fish | source
    zoxide init --cmd c --hook pwd fish | source

    abbr --add --global ls 'exa --color=always'
    abbr --add --global cp 'cp -ivr'
    abbr --add --global mv 'mv -iv'
    abbr --add --global mkdir 'mkdir -pv'
    abbr --add --global rg 'rg --smart-case'
    abbr --add --global sshpi 'ssh pi@192.168.2.228'
    abbr --add --global leaguelutris 'sudo sysctl -w abi.vsyscall32=0'
end
function fish_greeting
	fortune -a | lolcat
end

set fish_greeting
#vim: ft=bash
